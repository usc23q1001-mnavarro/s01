name = "Marvin"
age = 21
occupation = "Web developer"
movie = "Fast and Furious"
movieRatring = 98.7

print("I am " + name + ", and I am " + str(age) + 
	" years old, I work as a " + occupation +
	" and my rating for " + movie + " is " +
	str(movieRatring) + "%")